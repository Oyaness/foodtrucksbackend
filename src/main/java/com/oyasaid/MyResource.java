package com.oyasaid;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;


@Path("foodTrucks")
public class MyResource {
	
	/**
	 * Method to get all nearby food trucks in a certain radius from a certain location point with given longitude and latitude.
	 * @param uriInfo
	 * @return list of trucks in JSON format that are in the given parameter
	 */
    @GET
    @Path("findTrucks")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findNearbyTrucks(@Context UriInfo uriInfo) {
	
    	
    	FoodTruckQueries ftq = new FoodTruckQueries();
    	TruckResponse response =ftq.getTrucksWithFilter(uriInfo);
    	
    	System.out.println(response.getTrucks().size());
    	ObjectMapper objectMapper = new ObjectMapper();
    	
    	try {
			String json = objectMapper.writeValueAsString(response.getTrucks());
			System.out.println(json);
			return Response.ok(json, MediaType.APPLICATION_JSON).header("Access-Control-Allow-Origin", "*").header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			return Response.status(401).entity(e.getMessage()). type("text/plain").header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT"). build();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response.status(402).entity(e.getMessage()). type("text/plain").header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT"). build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(403).entity(e.getMessage()). type("text/plain").header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT"). build();
		}
    }
}