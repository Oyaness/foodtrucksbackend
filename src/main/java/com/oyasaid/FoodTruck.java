package com.oyasaid;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import com.sun.jersey.api.client.GenericType;

/**
 * Class that represents the Food Truck data item. 
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class FoodTruck {
	public static final GenericType<List<FoodTruck>> LIST_TYPE = new GenericType<List<FoodTruck>>() {};
	private long locationId;
	private String applicant;
	private String facilityType;
	private long cnn;
	private String locationDescription;
	private String address;
	private String block;
	private String lot;
	private String permit;
	private String status;
	private String foodItems;
	private double x;
	private double y;
	private double latitude;
	private double longitude;
	private String schedule;
	private String daysHours;
	private Date noiSent;
	private Date approved;
	private String received;
	private int priorPermit;
	private Date expirationDate;
	private String city;
	private String locationAddress;
	private String locationZip;
	private String locationState;

	
	public FoodTruck() {
    }
	 @JsonCreator
	 public FoodTruck(@JsonProperty("objectid") long locationId,
	                  @JsonProperty("applicant") String applicant,
	                  @JsonProperty("facilitytype") String facilityType,
	                  @JsonProperty("cnn") long cnn,
	                  @JsonProperty("locationdescription") String locationDescription,
	                  @JsonProperty("address") String address,
	                  @JsonProperty("block") String block,
	                  @JsonProperty("lot") String lot,
	                  @JsonProperty("permit") String permit,
	                  @JsonProperty("status") String status,
	                  @JsonProperty("fooditems") String foodItems,
	                  @JsonProperty("x") double x,
	                  @JsonProperty("y") double y,
	                  @JsonProperty("latitude") double latitude,
	                  @JsonProperty("longitude") double longitude,
	                  @JsonProperty("schedule") String schedule,
	                  @JsonProperty("dayshours") String daysHours,
	                  @JsonProperty("noisent") Date noiSent,
	                  @JsonProperty("approved") Date approved,
	                  @JsonProperty("received") String received,
	                  @JsonProperty("priorpermit") int priorPermit,
	                  @JsonProperty("expirationdate") Date expirationDate,
	                  @JsonProperty("location_city") String city,
	                  @JsonProperty("location_address") String locationAddress,
	                  @JsonProperty("location_zip") String locationZip,
	                  @JsonProperty("location_state") String locationState)
	    {
	        this.locationId = locationId;
	        this.applicant = applicant;
	        this.facilityType = facilityType;
	        this.cnn = cnn;
	        this.locationDescription = locationDescription;
	        this.address = address;
	        this.block = block;
	        this.lot = lot;
	        this.permit = permit;
	        this.status = status;
	        this.foodItems = foodItems;
	        this.x = x;
	        this.y = y;
	        this.latitude = latitude;
	        this.longitude = longitude;
	        this.schedule = schedule;
	        this.daysHours = daysHours;
	        this.noiSent = noiSent;
	        this.approved = approved;
	        this.received = received;
	        this.priorPermit = priorPermit;
	        this.expirationDate = expirationDate;
	        this.city = city;
	        this.locationAddress = locationAddress;
	        this.locationZip = locationZip;
	        this.locationState = locationState;
	    }

	@JsonProperty("objectid")
	public long getLocationId() {
		return locationId;
	}

	public String getApplicant() {
		return applicant;
	}
	
	@JsonProperty("facilitytype")
	public String getFacilityType() {
		return facilityType;
	}

	public long getCnn() {
		return cnn;
	}

	@JsonProperty("locationdescription")
	public String getLocationDescription() {
		return locationDescription;
	}

	public String getAddress() {
		return address;
	}

	public String getBlock() {
		return block;
	}

	public String getLot() {
		return lot;
	}

	public String getPermit() {
		return permit;
	}

	public String getStatus() {
		return status;
	}

	@JsonProperty("fooditems")
	public String getFoodItems() {
		return foodItems;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public String getSchedule() {
		return schedule;
	}

	@JsonProperty("dayshours")
	public String getDaysHours() {
		return daysHours;
	}

	@JsonProperty("noisent")
	public Date getNoiSent() {
		return noiSent;
	}

	public Date getApproved() {
		return approved;
	}

	public String getReceived() {
		return received;
	}

	@JsonProperty("priorpermit")
	public int getPriorPermit() {
		return priorPermit;
	}

	@JsonProperty("expirationdate")
	public Date getExpirationDate() {
		return expirationDate;
	}

	@JsonProperty("location_city")
	public String getCity() {
		return city;
	}

	@JsonProperty("location_address")
	public String getLocationAddress() {
		return locationAddress;
	}

	@JsonProperty("location_zip")
	public String getLocationZip() {
		return locationZip;
	}

	@JsonProperty("location_state")
	public String getLocationState() {
		return locationState;
	}
	
	public static List<String> getAllAttributes(){
		return Arrays.asList("objectid", "applicant", "facilitytype", "cnn", "locationdescription", "address", 
				"block", "lot", "permit", "status", "fooditems", "x", "y", "latitude", "longitude", "schedule"
				, "dayshours", "noisent", "approved", "received", "priorpermit", "expirationdate", "location_city", 
				"location_address", "location_zip", "location_state");
	}
}