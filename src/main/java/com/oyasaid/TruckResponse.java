package com.oyasaid;

import java.util.List;

public class TruckResponse {
	private List<FoodTruck> trucks;
	private int status;
	private String errorMessage;
	
	public TruckResponse(){}
	
	public TruckResponse(int status, String errorMessage, List<FoodTruck> trucks) {
		this.trucks = trucks;
		this.status = status;
		this.errorMessage = errorMessage;
	}
	
	
	public List<FoodTruck> getTrucks() {
		return trucks;
	}

	public void setTrucks(List<FoodTruck> trucks) {
		this.trucks = trucks;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	@Override
	public String toString() {
		return "trucks:" + trucks + ","+
			   "status:" + status + ","+
			   "errorMessage:" + errorMessage;
	}
}
