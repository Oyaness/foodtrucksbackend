package com.oyasaid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang3.StringUtils;

import com.socrata.api.Soda2Consumer;
import com.socrata.builders.SoqlQueryBuilder;
import com.socrata.model.soql.SoqlQuery;


public class FoodTruckQueries {
	private static final String databaseId = "6a9r-agq8";
	private static final String databaseUrl = "https://data.sfgov.org";
	private static final double earthRadius = 6371.01;
	
	private double longitude;
	private double latitude;
	private double radius;//in meters
	
	private ArrayList<String> queries;
	
	public FoodTruckQueries(){}
	
	/**
	 * Method that checks if the input parameters are valid.
	 * @param uriInfo
	 * @return true / false  
	 */
	private Boolean validFilter(UriInfo uriInfo){
		MultivaluedMap<String, String> params = uriInfo.getQueryParameters();
		if (!params.keySet().contains("lgt") || !params.keySet().contains("lat")) {
			System.out.println("No lat or lgt");
			return false;
		}
        for (String key : params.keySet()) {
        	switch (key) {
			case "lgt":
			case "lat":
			case "radius":
				if (!isNumber(params.getFirst(key))) {
					System.out.println("Invalid, lgt, lat or radius");
					return false;
				}
				break;
			default:
				if (!FoodTruck.getAllAttributes().contains(key)) {
					System.out.println("Invalid filter");
					return false;
				}
				break;
			}
        }
		return true;
	}
	
	/**
	 * Method gets called if all the parameters are valid. This method handles them accordingly.
	 * @param uriInfo
	 */
	private void handleParams(UriInfo uriInfo){
		queries = new ArrayList<>();
    	MultivaluedMap<String, String> params = uriInfo.getQueryParameters();
        for (String key : params.keySet()) {
        	switch (key) {
			case "lgt":
				longitude = Double.parseDouble(params.getFirst(key));
				break;
			case "lat":
				latitude = Double.parseDouble(params.getFirst(key));
				break;
			case "radius":
				radius = Double.parseDouble(params.getFirst(key));
				break;
			default:
				makeFilterQuery(key, stringToList(params.getFirst(key)));
				break;
			}
        }	
	}
	
	/**
	 * Method used to get the trucks with the appropriate filter.
	 * @param uriInfo
	 * @return a response with status code, message and list of trucks
	 */
	public TruckResponse getTrucksWithFilter(UriInfo uriInfo){
		if(validFilter(uriInfo)){
			System.out.println("Is valid url");
			handleParams(uriInfo);
			SoqlQueryBuilder queryBuilder = new SoqlQueryBuilder();
			
			String queryString = StringUtils.join(queries, " AND ");
			System.out.println(queryString);
			SoqlQuery query;
			if (queryString.length()>0 && radius != 0) {
				queryBuilder.setWhereClause(queryString.toString());
				query= queryBuilder.build();
				System.out.println(query.toString());
			}
			else{
				query = SoqlQuery.SELECT_ALL;
			}
			return getTrucks(query);
		}
		else{
			System.out.println("Url not valid");
			return new TruckResponse(400,"Bad Request", null);
		}
	}
	
	/**
	 * Method uses a filter to filter out trucks from the database and return them in a list.
	 * @param query, a filter to get the correct trucks
	 * @return list of food trucks
	 */
	private TruckResponse getTrucks(SoqlQuery query){
		try{
			Soda2Consumer consumer = Soda2Consumer.newConsumer(databaseUrl);
			
			return new TruckResponse(200, "OK", filterNearbyTrucks(consumer.query(databaseId, query, FoodTruck.LIST_TYPE)));
		}
		catch (Exception e){
			System.out.println(e.getMessage());
			return new TruckResponse(500, "Internal Server Error", null);
		}
	}

	/**
	 * Method that gets a list of trucks and returns the trucks that are in a certain radius.
	 * @param trucks All the trucks
	 * @return Filtered nearby trucks
	 */
	private List<FoodTruck> filterNearbyTrucks(List<FoodTruck> trucks) {
		System.out.println("Size of all trucks:"+trucks.size());
		List<FoodTruck> nearbyTrucks = new ArrayList<>();
//		//acos(sin(1.3963) * sin(Lat) + cos(1.3963) * cos(Lat) * cos(Lon - (-0.6981))) * 6371 <= 1000;
		for (FoodTruck truck : trucks) {
			if(Math.acos(Math.sin(Math.toRadians(latitude))*Math.sin(Math.toRadians(truck.getLatitude()))+Math.cos(Math.toRadians(latitude))*Math.cos(Math.toRadians(truck.getLatitude()))*Math.cos(Math.toRadians(truck.getLongitude())-Math.toRadians(longitude)))*earthRadius<=radius){
				nearbyTrucks.add(truck);
			}
		}
		System.out.println("filtered:"+nearbyTrucks.size());
		return nearbyTrucks;
	}
	
	/**
	 * Method that checks if a string is a number (positive, negative, integer, double) by using a regular expression
	 * @param str, the string being checked
	 * @return true/false value
	 */
	private Boolean isNumber(String str){
		return str.matches("-?\\d+(\\.\\d+)?");
	}
	 
	private List<String> stringToList(String str){
		return Arrays.asList(str.split(","));
	}
	 
	private void makeFilterQuery(String key, List<String> values){
		StringBuilder querySB = new StringBuilder();

		querySB.append("(");
		for (String value : values) {
			querySB.append(key+" LIKE \"%"+value+"%\" OR ");
		}
		querySB.delete(querySB.length()-4, querySB.length());
		querySB.append(")");

		queries.add(querySB.toString());
	} 
}
