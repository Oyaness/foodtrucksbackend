package com.oyasaid;

import javax.ws.rs.core.Application;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;

import com.oyasaid.MyResource;

public class MyResourceTest extends JerseyTest {

    @Override
    protected Application configure() {
        return new ResourceConfig(MyResource.class);
    }

    /**
     * Test to see that the message "Got it!" is sent in the response.
     */
//    @Test
//    public void testGetIt() {
//        final String responseMsg = target().path("foodTrucks/findTrucks").request().get(String.class);
//
//        assertEquals("Hello, Heroku!", responseMsg);
//    }
}
